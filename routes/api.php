<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\InstantController;
use App\Http\Controllers\Api\SavingsController;
use App\Http\Controllers\Api\EditController;
use App\Http\Controllers\Api\NotificationsController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\PromoHistoryController;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/user', [AuthController::class, 'user']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::group(['namespace' => 'Api', 'prefix' => 'instant'], function(){
    Route::get('/promos', [InstantController::class, 'promos'])->middleware('auth:api');
    Route::get('/stores', [InstantController::class, 'stores'])->middleware('auth:api');
	Route::post('/create', [InstantController::class, 'create'])->middleware('auth:api');
    Route::patch('/update-promo', [InstantController::class, 'update'])->middleware('auth:api');
    Route::patch('/delete-promo', [InstantController::class, 'delete'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'savings'], function(){
    Route::get('/promos', [SavingsController::class, 'promos'])->middleware('auth:api');
    Route::get('/promos/goods/{id}', [SavingsController::class, 'promosGoods'])->middleware('auth:api');
    Route::get('/promos/gifts/{id}', [SavingsController::class, 'promosGifts'])->middleware('auth:api');
    Route::get('/stores', [SavingsController::class, 'stores'])->middleware('auth:api');
    Route::get('/categories', [SavingsController::class, 'categories'])->middleware('auth:api');
    Route::post('/goods', [SavingsController::class, 'goods'])->middleware('auth:api');
	Route::post('/create', [SavingsController::class, 'create'])->middleware('auth:api');
    Route::patch('/update-promo', [SavingsController::class, 'update'])->middleware('auth:api');
    Route::patch('/delete-promo', [SavingsController::class, 'delete'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'promo-history'], function(){
    Route::get('/', [PromoHistoryController::class, 'history'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'edit'], function(){
    Route::get('/products', [EditController::class, 'products'])->middleware('auth:api');
    Route::get('/categories', [EditController::class, 'categories'])->middleware('auth:api');
    Route::patch('/update-product', [EditController::class, 'updateProduct'])->middleware('auth:api');
    Route::patch('/update-category', [EditController::class, 'updateCategory'])->middleware('auth:api');
    Route::post('/create-image', [EditController::class, 'createImage'])->middleware('auth:api');
    Route::delete('/delete-image', [EditController::class, 'deleteImage'])->middleware('auth:api');
    Route::get('/stores', [EditController::class, 'stores'])->middleware('auth:api');
    Route::get('/categories-stores', [EditController::class, 'categoriesStores'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'notifications'], function(){
    Route::get('/', [NotificationsController::class, 'list'])->middleware('auth:api');
    Route::post('/', [NotificationsController::class, 'create'])->middleware('auth:api');
    Route::patch('/', [NotificationsController::class, 'update'])->middleware('auth:api');
    Route::delete('/', [NotificationsController::class, 'delete'])->middleware('auth:api');
    Route::patch('/update-token', [NotificationsController::class, 'updateToken'])->middleware('auth:api');
    Route::post('/send-notification', [NotificationsController::class, 'sendNotification'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'faq'], function(){
    Route::get('/', [FaqController::class, 'list'])->middleware('auth:api');
    Route::post('/', [FaqController::class, 'create'])->middleware('auth:api');
    Route::patch('/', [FaqController::class, 'update'])->middleware('auth:api');
    Route::delete('/', [FaqController::class, 'delete'])->middleware('auth:api');
});

Route::group(['namespace' => 'Api', 'prefix' => 'users'], function(){
    Route::get('/', [UsersController::class, 'list'])->middleware('auth:api');
    Route::post('/', [UsersController::class, 'create'])->middleware('auth:api');
    Route::patch('/', [UsersController::class, 'update'])->middleware('auth:api');
    Route::delete('/', [UsersController::class, 'delete'])->middleware('auth:api');
});
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmUsers extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_users';

    protected $fillable = [
        'id',
        'username',
        'first_name',
        'last_name',
        'display_name',
        'phone',
        'email',
        'author_id',
        'latitude',
        'longitude',
        'device_type',
        'user_status',
        'city_id',
        'uuid',
        'fb_device_token'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmPromoHistory extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_promo_history';

    protected $fillable = [
        'id',
        'username',
        'promo_id',
        'promo_title',
        'created_at',
        'ip_address'
    ];
}

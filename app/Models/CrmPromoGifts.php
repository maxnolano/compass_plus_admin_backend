<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmPromoGifts extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_promo_gifts';

    protected $fillable = [
        'id',
        'promo_id',
        'gift_type',
        'product_category',
        'product_id',
        'amount',
        'amount_unit',
        'expire_date',
        'expire_days',
        'max_value',
        'max_value_unit'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmPromoStores extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_promo_stores';

    protected $fillable = [
        'id',
        'promo_id',
        // 'store_id'
    ];
}

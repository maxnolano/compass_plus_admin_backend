<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCategoriesForGas extends Model
{
    use HasFactory;

    protected $table = 'crm_categories';

    protected $fillable = [
        'id',
        'title',
        'parent_group_id'
    ];

    public function parent()
    {
        return $this->belongsTo($this, 'parent_category_id');
    }

    public function children()
    {
        return $this->hasMany($this, 'parent_category_id')->where('disabled', '=', '1');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }
}

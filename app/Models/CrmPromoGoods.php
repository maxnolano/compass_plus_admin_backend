<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmPromoGoods extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_promo_goods';

    protected $fillable = [
        'id',
        'promo_id',
        'category_id',
        'item_code',
        'amount_to_order',
        'amount_unit',
        'products',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmPromo extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_promo';

    protected $fillable = [
        'id',
        'title',
        'description',
        'expire_date',
        'status',
        'promo_type'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmNotification extends Model
{
    use HasFactory;

    use Orderable;

    protected $table = 'crm_notification';

    protected $fillable = [
        'id',
        'message'
    ];
}

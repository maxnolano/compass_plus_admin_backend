<?php

namespace App\Repositories\PromoHistory;

use App\Models\CrmPromo;
use App\Models\CrmPromoHistory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\PromoHistoryRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Facades\Auth;

class PromoHistoryRepository implements PromoHistoryRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of promo histories
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function history(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_promo_history as prh')
            ->where('prh.id', 'like', '%'.$request['id'].'%')
            ->where('prh.username', 'like', '%'.$request['username'].'%')
            ->where('prh.promo_id', 'like', '%'.$request['promo_id'].'%')
            ->where('prh.promo_title', 'like', '%'.$request['promo_title'].'%')
            ->where('prh.created_at', 'like', '%'.$request['created_at'].'%')
            ->where('prh.ip_address', 'like', '%'.$request['ip_address'].'%')
            ->select(
                'prh.id as id',
                'prh.username as username',
                'prh.promo_id as promo_id',
                'prh.promo_title as promo_title',
                'prh.created_at as created_at',
                'prh.ip_address as ip_address',
            )
            ->orderBy('prh.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Create promo history
     * @param Array $request
     * @param Object $result
     * @return Object
     */
    public function create(Array $request, Array $result, String $ip_address): array
    {
        $created_status = CrmPromoHistory::create([
            'username' => Auth::user()->username,
            'promo_id' => $result['created_status']['id'],
            'promo_title' => $result['created_status']['title'],
            'ip_address' => $ip_address,
        ]);

        return ['created_status' => $created_status];
    }
}
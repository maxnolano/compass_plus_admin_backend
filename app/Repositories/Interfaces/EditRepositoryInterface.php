<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface EditRepositoryInterface
{
    /**
     * Getting list of products
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function products(Array $request): LengthAwarePaginator;

    /**
     * Getting list of categories
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function categories(Array $request): EloquentCollection;

    /**
     * Getting list of stores
     * @param Array $request
     * @return Object
     */
    public function stores(Array $request): Object;

    /**
     * Getting list of categroies-stores
     * @param Array $request
     * @return Object
     */
    public function categoriesStores(Array $request): Object;
}
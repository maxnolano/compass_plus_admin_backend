<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

interface PromoHistoryRepositoryInterface
{
    /**
     * Getting list of promo histories
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function history(Array $request): LengthAwarePaginator;

    /**
     * Create promo history
     * @param Array $request
     * @param Object $result
     * @return Object
     */
    public function create(Array $request, Array $result, String $ip_address): array;
}
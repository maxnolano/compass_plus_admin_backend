<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface FaqRepositoryInterface
{
    /**
     * Getting list of faq
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator;

    /**
     * Create faq
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array;

    /**
     * Uodate faq
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array;

    /**
     * Delete faq
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array;
}
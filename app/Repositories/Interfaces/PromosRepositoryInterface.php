<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

interface PromosRepositoryInterface
{
    /**
     * Getting list of promos
     * @param Array $request
     * @param int $promo_type
     * @return LengthAwarePaginator
     */
    public function promos(Array $request, int $promo_type): LengthAwarePaginator;

    /**
     * Getting list of stores
     * @return Object
     */
    public function stores(): Object;

    /**
     * Getting list of categories
     * @return Object
     */
    public function categories(): array;

    /**
     * Getting list of goods
     * @param Array $request
     * @return Object
     */
    public function goods(Array $request): Object;

    /**
     * Create promo
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array;

    /**
     * Create promo goods and gifts
     * @param Array $goods
     * @param Array $gifts
     * @param int $promo_id
     * @return Object
     */
    public function createGoodsAndGifts(Array $goods, Array $gifts, $promo_id): array;
}
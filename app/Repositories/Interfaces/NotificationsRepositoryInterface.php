<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface NotificationsRepositoryInterface
{
    /**
     * Getting list of notifications
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator;

    /**
     * Create notification
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array;

    /**
     * Uodate notification
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array;

    /**
     * Delete notification
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array;

     /**
     * Uodate token
     * @param Array $request
     * @return Object
     */
    public function updateToken(Array $request): array;
}
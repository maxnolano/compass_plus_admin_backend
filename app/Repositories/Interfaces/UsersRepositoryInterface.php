<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface UsersRepositoryInterface
{
    /**
     * Getting list of users
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator;

    /**
     * Create user
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array;

    /**
     * Uodate user
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array;

    /**
     * Delete user
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array;
}
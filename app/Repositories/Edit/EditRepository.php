<?php


namespace App\Repositories\Edit;

use App\Models\CrmProducts;
use App\Models\CrmCategories;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\EditRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class EditRepository implements EditRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of products
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function products(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_products as pr')
            ->join('crm_categories as ct', 'pr.category_id', 'like', 'ct.id')
            ->where('pr.id', 'like', '%'.$request['id'].'%')
            ->where('pr.title', 'like', '%'.$request['title_p'].'%')
            ->where('pr.item_code', 'like', '%'.$request['item_code'].'%')
            // ->where('pr.category_id', 'like', '%'.$request['category_id'].'%')
            ->where('pr.category_id', ($request['category_id'] ? '=' : 'like'), ($request['category_id'] ? $request['category_id'] : '%'.$request['category_id'].'%'))
            ->where('pr.description', 'like', '%'.$request['description'].'%')
            ->where('pr.price', 'like', '%'.$request['price'].'%')
            ->where('pr.quantity', 'like', '%'.$request['quantity'].'%')
            ->where('pr.weight', 'like', '%'.$request['weight'].'%')
            ->where('pr.weight_unit', 'like', '%'.$request['weight_unit'].'%')
            ->where('pr.ean13', 'like', '%'.$request['ean13'].'%')
            ->select(
                'pr.id as id',
                'pr.title as title_p',
                'pr.item_code as item_code',
                'ct.title as category_title',
                'pr.category_id as category_id',
                'pr.description as description',
                'pr.price as price',
                'pr.quantity as quantity',
                'pr.weight as weight',
                'pr.weight_unit as weight_unit',
                'pr.ean13 as ean13',
                'pr.img as img',
            )
            ->orderBy('pr.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Getting list of categories
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function categories(Array $request): EloquentCollection
    {
        $categories = CrmCategories::where('parent_category_id', '=', '160')->where('disabled', '=', '0')->with('childrenRecursive')->orderBy('id', 'desc')->get();

        return $categories;
    }

    /**
     * Update product
     * @param Array $request
     * @return Object
     */
    public function updateProduct(Array $request): array
    {
        $product_updated_status = DB::table('crm_products')
            ->where('id', $request['id'])
            ->update(['title' => $request['title_p'], 'item_code' => $request['item_code'], 'category_id' => $request['category_id'], 'description' => $request['description'], 'price' => $request['price'], 'quantity' => $request['quantity'], 'weight' => $request['weight'], 'weight_unit' => $request['weight_unit'], 'ean13' => $request['ean13']]);

        return ['product_updated_status' => $product_updated_status];
    }

    /**
     * Update category
     * @param Array $request
     * @return Object
     */
    public function updateCategory(Array $request): array
    {
        $category_updated_status = DB::table('crm_categories')
            ->where('id', $request['id_c'])
            ->update(['title' => $request['title_c']]);

        $stores = DB::table('crm_stores')
            ->get()
            ->toArray();

        $categories = DB::table('crm_categories as ct')
            ->where('ct.disabled', '=', 0)
            ->where('ct.deleted', '=', 0)
            ->get()
            ->toArray();

        if(count($stores) != 0){
            foreach($stores as $store){
                if(in_array($store->id, $request['stores_c'])){
                    $row = DB::table('crm_categories_stores')
                        ->where('store_id', $store->id)
                        ->where('category_id', $request['id_c'])
                        ->get();
                    if(count($row) == 0){
                        DB::table('crm_categories_stores')->insert([
                            'store_id' => $store->id,
                            'category_id' => $request['id_c']
                        ]);
                    }
                }else{
                    $row = DB::table('crm_categories_stores')
                        ->where('store_id', $store->id)
                        ->get();
                    if(count($row) != 0){
                        DB::table('crm_categories_stores')
                            ->where('store_id', $store->id)
                            ->where('category_id', $request['id_c'])
                            ->delete();
                    }
                }
            }
        }

        return ['category_updated_status' => $category_updated_status];
    }

    /**
     * Create image
     * @param Array $request
     * @return Object
     */
    public function createImage(Array $request): array
    {
        $image_created_status = '';

        $id = $request['id'];
        $file = $request['img'];
        $name = date('Y-m-d_H:i:s_') . $request['name'];

        $imgIsNull = DB::table('crm_products as pr')
            ->where('pr.id', '=', $id)
            ->select(
                'pr.img as img'
            )
            ->get()
            ;

        $img_address = $name;
        

        if($imgIsNull[0]->img == ""){
            $image_created_status = DB::table('crm_products')
                ->where('id', $request['id'])
                ->update(['img' => $img_address]);

            $file->storeAs('public', $name);
        }else{
            if(file_exists(storage_path('/app/public/' . $imgIsNull[0]->img))){
                unlink(storage_path('/app/public/' . $imgIsNull[0]->img));
            }
            
            $image_created_status = DB::table('crm_products')
                ->where('id', $request['id'])
                ->update(['img' => $img_address]);

            $file->storeAs('public', $name);
        }

        return ['image_created_status' => [$image_created_status, $img_address]];
    }

    /**
     * Delete image
     * @param Array $request
     * @return Object
     */
    public function deleteImage(Array $request): array
    {
        $image_deleted_status = '';

        $id = $request['id'];

        $imgIsNull = DB::table('crm_products as pr')
            ->where('pr.id', '=', $id)
            ->select(
                'pr.img as img'
            )
            ->get()
            ;
        
        if($imgIsNull[0]->img != ""){
            unlink(storage_path('/app/public/' . $imgIsNull[0]->img));

            $image_deleted_status = DB::table('crm_products')
                ->where('id', $request['id'])
                ->update(['img' => ""]);
        }

        return ['image_deleted_status' => [$image_deleted_status]];
    }

    /**
     * Get stores for this endpoint
     * @param Array $request
     * @return Object
     */
    public function stores(Array $request): Object
    {
        return DB::table('crm_stores as s')
            // ->where('s.deleted', '=', '0')
            // ->where('s.disabled', '=', '0')
            ->select(
                's.title as label',
                's.id as id'
            )
            ->distinct()
            ->get()
            ;
    }

    /**
     * Get categories-stores for this endpoint
     * @param Array $request
     * @return Object
     */
    public function categoriesStores(Array $request): Object
    {
        return DB::table('crm_categories_stores as cs')
            // ->where('s.deleted', '=', '0')
            ->where('cs.category_id', '=', $request['id'])
            ->select(
                'cs.store_id',
                'cs.category_id as category_id'
            )
            ->get()
            ;
    }
}
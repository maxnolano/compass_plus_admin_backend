<?php


namespace App\Repositories\Users;

use App\Models\CrmUsers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\UsersRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};

class UsersRepository implements UsersRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of users
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_users as usr')
            ->where('usr.id', 'like', '%'.$request['id'].'%')
            ->where('usr.username', 'like', '%'.$request['username'].'%')
            ->where('usr.first_name', 'like', '%'.$request['first_name'].'%')
            ->where('usr.last_name', 'like', '%'.$request['last_name'].'%')
            ->where('usr.display_name', 'like', '%'.$request['display_name'].'%')
            ->where('usr.phone', 'like', '%'.$request['phone'].'%')
            ->where('usr.email', 'like', '%'.$request['email'].'%')
            ->select(
                'usr.id as id',
                'usr.username as username',
                'usr.first_name as first_name',
                'usr.last_name as last_name',
                'usr.display_name as display_name',
                'usr.phone as phone',
                'usr.email as email'
            )
            ->orderBy('usr.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Create user
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array
    {
        $created_status = CrmUsers::create([
            'username' => $request['username'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'display_name' => $request['display_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'author_id' => 1,//temporary value
            'latitude' => 0,//temporary value
            'longitude' => 0,//temporary value
            'device_type' => 2,//temporary value
            'user_status' => 1,//temporary value
            'city_id' => 2,//temporary value
            'uuid' => 'empty-value-change-here',//temporary value
        ]);

        return ['created_status' => $created_status];
    }

    /**
     * Update user
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $users_updated_status = DB::table('crm_users')
            ->where('id', $request['id'])
            ->update(['username' => $request['username'], 'first_name' => $request['first_name'], 'last_name' => $request['last_name'], 'display_name' => $request['display_name'], 'phone' => $request['phone'], 'email' => $request['email']]);

        return ['users_updated_status' => $users_updated_status];
    }

    /**
     * Delete user
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array
    {
        $users_deleted_status = DB::table('crm_users')
            ->where('id', $request['id'])
            ->delete();

        return ['users_deleted_status' => $users_deleted_status];
    }
}
<?php


namespace App\Repositories\Promos;

use App\Models\CrmPromo;
use App\Models\CrmPromoGoods;
use App\Models\CrmPromoGifts;
use App\Models\CrmPromoStores;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\PromosRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use App\Models\CrmCategories;
use App\Models\CrmCategoriesForGas;

class PromosRepository implements PromosRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of promos
     * @param Array $request
     * @param int $promo_type
     * @return LengthAwarePaginator
     */
    public function promos(Array $request, int $promo_type): LengthAwarePaginator
    {
        return DB::table('crm_promo as pr')
            // ->join('crm_promo_stores as st', 'pr.id', '=', 'st.promo_id')
            // ->join('crm_stores as s', 'st.store_id', '=', 's.id')
            ->where('pr.promo_type', '=', $promo_type)
            ->where('pr.id', 'like', '%'.$request['id'].'%')
            // ->where('st.store_id', 'like', '%'.$request['st_store_id'].'%')
            // ->where('s.title', 'like', '%'.$request['st_store_name'].'%')
            ->where('pr.title', 'like', '%'.$request['title'].'%')
            ->where('pr.description', 'like', '%'.$request['description'].'%')
            ->where('pr.expire_date', 'like', '%'.$request['expire_date'].'%')
            ->where('pr.status', 'like', '%'.$request['status'].'%')
            ->select(
                'pr.id as id',
                // 'st.store_id as st_store_id',
                // 's.title as st_store_name',
                'pr.title as title',
                'pr.description as description',
                'pr.expire_date as expire_date',
                'pr.status as status',
            )
            ->orderBy('pr.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * List of all promos goods
     * @param Array $request
     * @param int $promo_type
     * @return Object
     */
    public function promosGoods(Array $request, int $promo_type): Object
    {
        return DB::table('crm_promo_goods as prgo')
            ->join('crm_categories as ct', 'prgo.category_id', '=', 'ct.id')
            ->join('crm_products', function($join)
                {
                    $join->on('prgo.item_code', '=', 'crm_products.item_code');
                    $join->on('prgo.category_id', '=', 'crm_products.category_id');
                })
            ->where('prgo.promo_id', '=', trim($request['id']))
            ->select(
                'prgo.id as id',
                'prgo.promo_id as promo_id',
                'prgo.category_id as category_id',
                'ct.title as ct_title',
                'prgo.item_code as item_code',
                'crm_products.title as pr_title',
                'prgo.amount_to_order as amount_to_order',
                'prgo.amount_unit as amount_unit',
                'prgo.created_at as created_at',
                'prgo.updated_at as updated_at',
            )
            ->get();
    }

    /**
     * List of all promos gifts
     * @param Array $request
     * @param int $promo_type
     * @return Object
     */
    public function promosGifts(Array $request, int $promo_type): Object
    {
        return DB::table('crm_promo_gifts as prgi')
            ->join('crm_categories as ct', 'prgi.product_category', '=', 'ct.id')
            ->join('crm_products as pr', 'prgi.product_id', '=', 'pr.id')
            ->where('prgi.promo_id', '=', trim($request['id']))
            ->select(
                'prgi.id as id',
                'prgi.promo_id as promo_id',
                'prgi.product_category as product_category',
                'ct.title as ct_title',
                'prgi.product_id as product_id',
                'pr.title as pr_title',
                'prgi.amount as amount',
                'prgi.amount_unit as amount_unit',
                'prgi.created_at as created_at',
                'prgi.updated_at as updated_at',
                'prgi.gift_type as gift_type',
                'prgi.expire_date as expire_date',
                'prgi.expire_days as expire_days',
                'prgi.max_value as max_value',
                'prgi.max_value_unit as max_value_unit',
            )
            ->get();
    }

    /**
     * Getting list of stores
     * @return array
     */
    public function stores(): Object
    {
        return DB::table('crm_stores as st')
            ->select(
                'st.id as id',
                'st.title as title',
            )
            ->orderBy('st.id', 'desc')
            ->get()
            ;
    }

    /**
     * Getting list of categories
     * @return array
     */
    public function categories(): array
    {
        // return DB::table('crm_categories as ct')
        //     ->select(
        //         'ct.id as id',
        //         'ct.title as title',
        //     )
        //     ->orderBy('ct.id', 'desc')
        //     ->get()
        //     ;

        // $categories_for_gas = CrmCategoriesForGas::where('parent_category_id', '=', '162')->where('disabled', '=', '1')->where('title', '=', 'ГАЗ')->with('childrenRecursive')->orderBy('id', 'desc')->get();
        $categories_for_gas = CrmCategoriesForGas::where('parent_category_id', '=', '1')->where('disabled', '=', '0')->with('childrenRecursive')->orderBy('id', 'desc')->get();
        
        $categories = CrmCategories::where('parent_category_id', '=', '160')->where('disabled', '=', '0')->with('childrenRecursive')->orderBy('id', 'desc')->get();

        $merged = $categories->merge($categories_for_gas);

        $merged_result = $merged->all();

        return $merged_result;
    }

    /**
     * Getting list of goods
     * @param Array $request
     * @return array
     */
    public function goods(Array $request): Object
    {
        if(gettype($request['category']) == 'array'){
            return DB::table('crm_products as pr')
                ->whereIn('pr.category_id', $request['category'])
                ->select(
                    'pr.id as id',
                    'pr.title as title',
                    'pr.item_code as item_code'
                )
                ->orderBy('pr.id', 'desc')
                ->get()
                ;
        }else{
            return DB::table('crm_products as pr')
                ->where('pr.category_id', '=', $request['category'])
                ->select(
                    'pr.id as id',
                    'pr.title as title',
                    'pr.item_code as item_code'
                )
                ->orderBy('pr.id', 'desc')
                ->get()
                ;
        }
    }

    /**
     * Create promo
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array
    {
        $created_status = CrmPromo::create([
            'title' => $request['title'],
            'description' => $request['description'],
            'expire_date' => $request['expiration_date'],
            'status' => $request['status'],
            'promo_type' => $request['promo_type'],
        ]);

        // foreach($request['selected_stations'] as $r){
        //     CrmPromoStores::create([
        //         'promo_id' => $created_status->id,
        //         'store_id' => $r
        //     ]);
        // }

        return ['created_status' => $created_status];
    }

    /**
     * Update promo
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $promo_updated_status = DB::table('crm_promo')
            ->where('id', $request['id'])
            ->update(['title' => $request['title'], 'description' => $request['description'], 'expire_date' => $request['expiration_date'], 'status' => $request['status']]);

        return ['promo_updated_status' => $promo_updated_status];
    }

    /**
     * Delete promo
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array
    {
        $promo_deleted_status = DB::table('crm_promo')
            ->where('id', $request['id'])
            ->delete();

        $promo_gifts_deleted_status = DB::table('crm_promo_gifts')
            ->where('promo_id', $request['id'])
            ->delete();

        $promo_goods_deleted_status = DB::table('crm_promo_goods')
            ->where('promo_id', $request['id'])
            ->delete();

        return ['promo_deleted_status' => $promo_deleted_status,
            'promo_gifts_deleted_status' => $promo_gifts_deleted_status,
            'promo_goods_deleted_status' => $promo_goods_deleted_status,
        ];
    }

    /**
     * Create promo goods and gifts
     * @param Array $goods
     * @param Array $gifts
     * @param int $promo_id
     * @return Object
     */
    public function createGoodsAndGifts(Array $goodsTo, Array $giftsTo, $promo_id): array
    {
        $created_status_goods = null;
        $created_status_gifts = null;

        if($goodsTo){
            foreach($goodsTo as $good){

                if(count($good['category']) > 1){
                    $goods_ids = [];
                        
                    foreach($good['goods'] as $good_item){
                        $goods_ids[] = $good_item;
                    }

                    $goods_ids = implode(",",$goods_ids);

                    $created_status_goods = CrmPromoGoods::create([
                        'item_code' => -1,
                        'promo_id' => $promo_id,
                        'category_id' => -1,
                        'amount_to_order' => $good['amount'],
                        'amount_unit' => $good['unit'],
                        'products' => $goods_ids
                    ]);
                }else{
                    if($good['goods_together'] == true){
                        $goods_ids = [];
                        
                        foreach($good['goods'] as $good_item){
                            $goods_ids[] = $good_item;
                        }
    
                        $goods_ids = implode(",",$goods_ids);
    
                        $created_status_goods = CrmPromoGoods::create([
                            'item_code' => -1,
                            'promo_id' => $promo_id,
                            // 'category_id' => $good['category'][0],
                            'category_id' => -1,
                            'amount_to_order' => $good['amount'],
                            'amount_unit' => $good['unit'],
                            'products' => $goods_ids
                        ]);
                    }else{
                        foreach($good['goods'] as $good_item){
                            $created_status_goods = CrmPromoGoods::create([
                                'item_code' => $good_item,
                                'promo_id' => $promo_id,
                                'category_id' => $good['category'][0],
                                'amount_to_order' => $good['amount'],
                                'amount_unit' => $good['unit'],
                                'products' => $good_item
                            ]);
                        }
                    }
                }
            }
        }
        if($giftsTo){
            foreach($giftsTo as $gift){
                $gift_type_to_put = '';
                if($gift['gift_type'] == 1){
                    $gift_type_to_put = 'g';
                }else{
                    $gift_type_to_put = 'd';
                }
                $$created_status_gifts = CrmPromoGifts::create([
                    'promo_id' => $promo_id,
                    'gift_type' => $gift['gift_type'],
                    'expire_date' => $gift['expire_date_gifts'],
                    'product_category' => $gift['category_gifts_'.$gift_type_to_put.''][0],
                    'product_id' => $gift['goods_gifts_'.$gift_type_to_put.''],
                    'amount' => $gift['amount_gifts_'.$gift_type_to_put.''],
                    'amount_unit' => $gift['unit_gifts_'.$gift_type_to_put.''],
                    'expire_days' => $gift['expire_days'],
                    'max_value' => $gift['max_value'],
                    'max_value_unit' => $gift['max_value_unit'],
                ]);
            }
        }

        return ['created_status_goods' => $created_status_goods, 'created_status_gifts' => $created_status_gifts,];
    }
}
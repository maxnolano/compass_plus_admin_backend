<?php


namespace App\Repositories\Notifications;

use App\Models\CrmNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\NotificationsRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};
use Illuminate\Support\Facades\Auth;

class NotificationsRepository implements NotificationsRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of notifications
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_notification as nt')
            ->where('nt.id', 'like', '%'.$request['id'].'%')
            ->where('nt.message', 'like', '%'.$request['message'].'%')
            ->select(
                'nt.id as id',
                'nt.message as message'
            )
            ->orderBy('nt.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Create notification
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array
    {
        $created_status = CrmNotification::create([
            'message' => $request['message']
        ]);

        return ['created_status' => $created_status];
    }

    /**
     * Update notification
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $notification_updated_status = DB::table('crm_notification')
            ->where('id', $request['id'])
            ->update(['message' => $request['message']]);

        return ['notification_updated_status' => $notification_updated_status];
    }

    /**
     * Delete notification
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array
    {
        $notification_deleted_status = DB::table('crm_notification')
            ->where('id', $request['id'])
            ->delete();

        return ['notification_deleted_status' => $notification_deleted_status];
    }

    /**
     * Update token
     * @param Array $request
     * @return Object
     */
    public function updateToken(Array $request): array
    {
        try{
            $token_updated_status = DB::table('crm_users')
                ->where('id', Auth::user()->id)
                ->update(['fb_device_token' => $request['token']]);

            return ['notification_updated_status' => $token_updated_status];
        }catch(\Exception $e){
            return ['Some error while updating token' => $e];
        }
    }
}
<?php


namespace App\Repositories\Faq;

use App\Models\CrmFaq;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\FaqRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};

class FaqRepository implements FaqRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of faq
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function list(Array $request): LengthAwarePaginator
    {
        return DB::table('crm_faq as fq')
            ->where('fq.id', 'like', '%'.$request['id'].'%')
            ->where('fq.question', 'like', '%'.$request['question'].'%')
            ->where('fq.answer', 'like', '%'.$request['answer'].'%')
            ->select(
                'fq.id as id',
                'fq.question as question',
                'fq.answer as answer',
            )
            ->orderBy('fq.id', 'desc')
            ->paginate(20)
            ;
    }

    /**
     * Create faq
     * @param Array $request
     * @return Object
     */
    public function create(Array $request): array
    {
        $created_status = CrmFaq::create([
            'question' => $request['question'],
            'answer' => $request['answer'],
            'disabled' => 0,
            'deleted' => 0,
            'lang' => ''
        ]);

        return ['created_status' => $created_status];
    }

    /**
     * Update faq
     * @param Array $request
     * @return Object
     */
    public function update(Array $request): array
    {
        $faq_updated_status = DB::table('crm_faq')
            ->where('id', $request['id'])
            ->update(['question' => $request['question'], 'answer' => $request['answer']]);

        return ['faq_updated_status' => $faq_updated_status];
    }

    /**
     * Delete faq
     * @param Array $request
     * @return Object
     */
    public function delete(Array $request): array
    {
        $faq_deleted_status = DB::table('crm_faq')
            ->where('id', $request['id'])
            ->delete();

        return ['faq_deleted_status' => $faq_deleted_status];
    }
}
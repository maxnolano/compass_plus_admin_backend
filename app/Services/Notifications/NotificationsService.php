<?php

namespace App\Services\Notifications;

use App\Repositories\Notifications\NotificationsRepository;
use App\Repositories\Interfaces\NotificationsRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmNotification as NotificationResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use  App\Models\CrmUsers;
use Notification;
use App\Notifications\SendPushNotification;

/**
 * Service for notifications
 */
class NotificationsService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for notifications
    private NotificationsRepositoryInterface $notificationsRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // NotificationsRepository initialization
        $this->notificationsRepository = new NotificationsRepository();

        $this->client = new Client();
    }

    /**
     * List of notifications
     * @return Object
     */
    public function list(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['message'] = $this->request->message;

            $notifications = $this->notificationsRepository->list($request_params);
            return $this->result(NotificationResource::collection($notifications));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Create notification
     * @return Object
     */
    public function create(): array
    {
        try{
            $request_params = [];
            $request_params['message'] = $this->request->obj['message'];

            $result = $this->notificationsRepository->create($request_params);

            // $firebaseToken = CrmUsers::where('id', 273)->pluck('fb_device_token')->all();
            $firebaseToken = CrmUsers::whereNotNull('fb_device_token')->pluck('fb_device_token')->all();
            
            $SERVER_API_KEY = env('FIREBASE_SERVER_KEY');
        
            $data = [
                "registration_ids" => $firebaseToken,
                "notification" => [
                    "title" => 'Test title (laravel)',
                    "body" => $request_params['message'],
                ]
            ];
            $dataString = json_encode($data);
        
            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];
        
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                    
            $response = curl_exec($ch);
        
            return $this->result(['data' => [$response, $result['created_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update notification
     * @return Object
     */
    public function update(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $request_params['message'] = $this->request['message'];
            $result = $this->notificationsRepository->update($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Delete notification
     * @return Object
     */
    public function delete(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $result = $this->notificationsRepository->delete($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update token
     * @return Object
     */
    public function updateToken(): array
    {
        try{
            $request_params = [];
            $request_params['token'] = $this->request->obj['token'];

            $result = $this->notificationsRepository->updateToken($request_params);

            return $this->result(['data' => [$result['created_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Send notification
     * @return Object
     */
    public function sendNotification(): array
    {
        try{
            $request_params = [];
            $request_params['title'] = $this->request->obj['title'];
            $request_params['message'] = $this->request->obj['message'];

            $fcmTokens = CrmUsers::whereNotNull('fb_device_token')->pluck('fb_device_token')->toArray();
            // $fcmTokens = CrmUsers::where('id', 270)->pluck('fb_device_token')->toArray();
    
            Notification::send(null,new SendPushNotification($request_params['title'], $request_params['message'], $fcmTokens));

            return $this->result(['data' => ['Notification Sent Successfully!!']]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
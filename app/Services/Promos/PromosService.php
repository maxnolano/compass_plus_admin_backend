<?php

namespace App\Services\Promos;

use App\Repositories\Promos\PromosRepository;
use App\Repositories\PromoHistory\PromoHistoryRepository;
use App\Repositories\Interfaces\PromosRepositoryInterface;
use App\Repositories\Interfaces\PromoHistoryRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmPromo as PromoResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for getting datas from orders_retalix table
 */
class PromosService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for retalix orders
    private PromosRepositoryInterface $promosRepository;

    // Repository for promo history
    private PromoHistoryRepositoryInterface $promoHistoryRepository;

    // GuzzleHttp client example
    protected Client $client;

    private int $global_promo_id;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // RetalixRepository initialization
        $this->promosRepository = new PromosRepository();

        // PromoHistoryRepo initialization
        $this->promoHistoryRepository = new PromoHistoryRepository();

        $this->client = new Client();
    }

    /**
     * List of all promos
     * @param int $promo_type
     * @return Object
     */
    public function promos(int $promo_type): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            // $request_params['st_store_id'] = $this->request->st_store_id;
            // $request_params['st_store_name'] = $this->request->st_store_name;
            $request_params['title'] = $this->request->title;
            $request_params['description'] = $this->request->description;
            $request_params['expire_date'] = $this->request->expire_date;
            $request_params['status'] = $this->request->status;

            $promos = $this->promosRepository->promos($request_params, $promo_type);
            return $this->result(PromoResource::collection($promos));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of all promos goods
     * @param int $promo_type
     * @return Object
     */
    public function promosGoods(int $promo_type): Array
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;

            $promosGoods = $this->promosRepository->promosGoods($request_params, $promo_type);
            return $this->result(['data' => $promosGoods]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of all promos gifts
     * @param int $promo_type
     * @return Object
     */
    public function promosGifts(int $promo_type): Array
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;

            $promosGifts = $this->promosRepository->promosGifts($request_params, $promo_type);
            return $this->result(['data' => $promosGifts]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of all stores
     * @return Object
     */
    public function stores(): array
    {
        try {
            $stores = $this->promosRepository->stores();
            return $this->result(['data' => $stores]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of all categories
     * @return Object
     */
    public function categories(): array
    {
        try {
            $categories = $this->promosRepository->categories();
            return $this->result(['data' => $categories]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of goods
     * @return Object
     */
    public function goods(): array
    {
        try {
            $request_params = [];
            $request_params['category'] = $this->request->category;

            $goods = $this->promosRepository->goods($request_params);
            return $this->result(['data' => $goods]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Create promo
     * @param int $promo_type
     * @return Object
     */
    public function create($promo_type): array
    {
        try{
            $request_params = [];
            // $request_params['selected_stations'] = $this->request->obj['selected_stations'];
            $request_params['title'] = $this->request->obj['title'];
            $request_params['description'] = $this->request->obj['description'];
            $request_params['expiration_date'] = $this->request->obj['expiration_date'];
            $request_params['status'] = $this->request->obj['status'];
            $request_params['promo_type'] = $promo_type;

            $result = $this->promosRepository->create($request_params);

            $this->global_promo_id = $result['created_status']['id'];

            $ip_address = $this->request->ip();

            $this->promoHistoryRepository->create($request_params, $result, $ip_address);

            $resultGoodsAndGifts = [];

            if($promo_type == 1){
                $resultGoodsAndGifts = $this->promosRepository->createGoodsAndGifts(!empty($this->request->obj['goods']) ? $this->request->obj['goods'] : [], !empty($this->request->obj['gifts']) ? $this->request->obj['gifts'] : [], $result['created_status']['id']);
            }

            if($promo_type == 1){
                return $this->result(['data' => [$result['created_status'], $resultGoodsAndGifts['created_status_goods'], $resultGoodsAndGifts['created_status_gifts']]]);
            }else{
                return $this->result(['data' => [$result['created_status']]]);
            }
        }catch(\Exception $e) {
            $result = $this->promosRepository->delete($this->global_promo_id);

            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update promo
     * @param int $promo_type
     * @return Object
     */
    public function update($promo_type): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $request_params['title'] = $this->request['title'];
            $request_params['description'] = $this->request['description'];
            $request_params['expiration_date'] = $this->request['expiration_date'];
            $request_params['status'] = $this->request['status'];
            $request_params['promo_type'] = $promo_type;

            $result = $this->promosRepository->update($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Delete promo
     * @param int $promo_type
     * @return Object
     */
    public function delete($promo_type): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];

            $result = $this->promosRepository->delete($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
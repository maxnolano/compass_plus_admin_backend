<?php

namespace App\Services\Faq;

use App\Repositories\Faq\FaqRepository;
use App\Repositories\Interfaces\FaqRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmFaq as FaqResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for faq
 */
class FaqService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for faq
    private FaqRepositoryInterface $faqRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // FaqRepository initialization
        $this->faqRepository = new FaqRepository();

        $this->client = new Client();
    }

    /**
     * List of faq
     * @return Object
     */
    public function list(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['question'] = $this->request->question;
            $request_params['answer'] = $this->request->answer;

            $faq = $this->faqRepository->list($request_params);
            return $this->result(FaqResource::collection($faq));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Create faq
     * @return Object
     */
    public function create(): array
    {
        try{
            $request_params = [];
            $request_params['question'] = $this->request->obj['question'];
            $request_params['answer'] = $this->request->obj['answer'];

            $result = $this->faqRepository->create($request_params);

            return $this->result(['data' => [$result['created_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update faq
     * @return Object
     */
    public function update(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $request_params['question'] = $this->request->question;
            $request_params['answer'] = $this->request->answer;
            $result = $this->faqRepository->update($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Delete faq
     * @return Object
     */
    public function delete(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $result = $this->faqRepository->delete($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
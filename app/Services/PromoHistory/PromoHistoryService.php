<?php

namespace App\Services\PromoHistory;

use App\Repositories\PromoHistory\PromoHistoryRepository;
use App\Repositories\Interfaces\PromoHistoryRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmPromoHistory as PromoHistoryResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for getting promo history
 */
class PromoHistoryService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for retalix orders
    private PromoHistoryRepositoryInterface $promoHistoryRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // RetalixRepository initialization
        $this->promoHistoryRepository = new PromoHistoryRepository();

        $this->client = new Client();
    }

    /**
     * List of all promo histories
     * @return Object
     */
    public function history(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['username'] = $this->request->username;
            $request_params['promo_id'] = $this->request->promo_id;
            $request_params['promo_title'] = $this->request->promo_title;
            $request_params['created_at'] = $this->request->created_at;
            $request_params['ip_address'] = $this->request->ip_address;

            $histories = $this->promoHistoryRepository->history($request_params);
            return $this->result(PromoHistoryResource::collection($histories));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
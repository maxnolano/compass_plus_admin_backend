<?php

namespace App\Services\Edit;

use App\Repositories\Edit\EditRepository;
use App\Repositories\Interfaces\EditRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmProducts as ProductsResource;
use App\Http\Resources\CrmCategories as CategoriesResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for getting datas from orders_retalix table
 */
class EditService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for retalix orders
    private EditRepositoryInterface $editRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // EditRepository initialization
        $this->editRepository = new EditRepository();

        $this->client = new Client();
    }

    /**
     * List of all products
     * @return Object
     */
    public function products(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['title_p'] = $this->request->title_p;
            $request_params['item_code'] = $this->request->item_code;
            $request_params['category_id'] = $this->request->category_id;
            $request_params['description'] = $this->request->description;
            $request_params['price'] = $this->request->price;
            $request_params['quantity'] = $this->request->quantity;
            $request_params['weight'] = $this->request->weight;
            $request_params['weight_unit'] = $this->request->weight_unit;
            $request_params['ean13'] = $this->request->ean13;
            $products = $this->editRepository->products($request_params);
            return $this->result(ProductsResource::collection($products));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * List of all categories
     * @return Object
     */
    public function categories(): Object
    {
        try {
            $request_params = [];
            $request_params['id_c'] = $this->request->id;
            $request_params['title_c'] = $this->request->title_c;
            $request_params['store_id_c'] = $this->request->store_id_c;
            $request_params['object_type_c'] = $this->request->object_type_c;
            $categories = $this->editRepository->categories($request_params);
            return $this->result($categories);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update product
     * @return Object
     */
    public function updateProduct(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['title_p'] = $this->request->title_p;
            $request_params['item_code'] = $this->request->item_code;
            $request_params['category_id'] = $this->request->category_id;
            $request_params['description'] = $this->request->description;
            $request_params['price'] = $this->request->price;
            $request_params['quantity'] = $this->request->quantity;
            $request_params['weight'] = $this->request->weight;
            $request_params['weight_unit'] = $this->request->weight_unit;
            $request_params['ean13'] = $this->request->ean13;

            $result = $this->editRepository->updateProduct($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update category
     * @return Object
     */
    public function updateCategory(): array
    {
        try{
            $request_params = [];
            $request_params['id_c'] = $this->request->id_c;
            $request_params['title_c'] = $this->request->title_c;
            $request_params['stores_c'] = $this->request->stores_c;

            $result = $this->editRepository->updateCategory($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Create image
     * @return Object
     */
    public function createImage(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['img'] = $this->request->file('img');
            $request_params['name'] = $this->request->name;

            $result = $this->editRepository->createImage($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Delete image
     * @return Object
     */
    public function deleteImage(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];

            $result = $this->editRepository->deleteImage($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get stores for this endpoint
     * @return Array
     */
    public function stores(): Array
    {
        try {
            $request_params = [];

            $result = $this->editRepository->stores($request_params);

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Get cateogories-stores for this endpoint
     * @return Array
     */
    public function categoriesStores(): Array
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request['id'];

            $result = $this->editRepository->categoriesStores($request_params);

            return $this->result(['data' => $result]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
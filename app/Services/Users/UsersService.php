<?php

namespace App\Services\Users;

use App\Repositories\Users\UsersRepository;
use App\Repositories\Interfaces\UsersRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use App\Http\Resources\CrmUsers as UsersResource;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};

/**
 * Service for users
 */
class UsersService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for users
    private UsersRepositoryInterface $usersRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // UsersRepository initialization
        $this->usersRepository = new UsersRepository();

        $this->client = new Client();
    }

    /**
     * List of users
     * @return Object
     */
    public function list(): Object
    {
        try {
            $request_params = [];
            $request_params['id'] = $this->request->id;
            $request_params['username'] = $this->request->username;
            $request_params['first_name'] = $this->request->first_name;
            $request_params['last_name'] = $this->request->last_name;
            $request_params['display_name'] = $this->request->display_name;
            $request_params['phone'] = $this->request->phone;
            $request_params['email'] = $this->request->email;

            $users = $this->usersRepository->list($request_params);
            return $this->result(UsersResource::collection($users));
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Create user
     * @return Object
     */
    public function create(): array
    {
        try{
            $request_params = [];
            $request_params['username'] = $this->request->obj['username'];
            $request_params['first_name'] = $this->request->obj['first_name'];
            $request_params['last_name'] = $this->request->obj['last_name'];
            $request_params['display_name'] = $this->request->obj['display_name'];
            $request_params['phone'] = $this->request->obj['phone'];
            $request_params['email'] = $this->request->obj['email'];

            $result = $this->usersRepository->create($request_params);

            return $this->result(['data' => [$result['created_status']]]);
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Update user
     * @return Object
     */
    public function update(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $request_params['username'] = $this->request->username;
            $request_params['first_name'] = $this->request->first_name;
            $request_params['last_name'] = $this->request->last_name;
            $request_params['display_name'] = $this->request->display_name;
            $request_params['phone'] = $this->request->phone;
            $request_params['email'] = $this->request->email;
            $result = $this->usersRepository->update($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    /**
     * Delete user
     * @return Object
     */
    public function delete(): array
    {
        try{
            $request_params = [];
            $request_params['id'] = $this->request['id'];
            $result = $this->usersRepository->delete($request_params);

            return $this->result(['data' => [$result]]);

        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}
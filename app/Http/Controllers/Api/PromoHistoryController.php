<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\PromoHistory\PromoHistoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PromoHistoryController extends ApiController
{
    /**
     * List of promo histories
     * @param Request $request
     * @return Object
     */
    public function history(Request $request): Object {
        $response = (new PromoHistoryService($request))->history();
        return $this->result($response);
    }
}
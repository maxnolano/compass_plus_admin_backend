<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Services\Edit\EditService;

class EditController extends ApiController
{
    /**
     * List of products
     * @param Request $request
     * @return Object
     */
    public function products(Request $request): Object {
        $response = (new EditService($request))->products();
        return $this->result($response);
    }
    
    /**
     * List of categories
     * @param Request $request
     * @return Object
     */
    public function categories(Request $request): Object {
        $response = (new EditService($request))->categories();
        return $this->result($response);
    }

    /**
     * Update product
     * @param Request $request
     * @return Object
     */
    public function updateProduct(Request $request): array {
        $response = (new EditService($request))->updateProduct();
        return $this->result($response);
    }

    /**
     * Update category
     * @param Request $request
     * @return Object
     */
    public function updateCategory(Request $request): array {
        $response = (new EditService($request))->updateCategory();
        return $this->result($response);
    }

    /**
     * Create image
     * @param Request $request
     * @return Object
     */
    public function createImage(Request $request): array {
        $response = (new EditService($request))->createImage();
        return $this->result($response);
    }

    /**
     * Delete image
     * @param Request $request
     * @return Object
     */
    public function deleteImage(Request $request): array {
        $response = (new EditService($request))->deleteImage();
        return $this->result($response);
    }

    /**
     * List of stores
     * @param Request $request
     * @return Object
     */
    public function stores(Request $request): array {
        $response = (new EditService($request))->stores();
        return $this->result($response);
    }

    /**
     * List of categories-stores
     * @param Request $request
     * @return Object
     */
    public function categoriesStores(Request $request): array {
        $response = (new EditService($request))->categoriesStores();
        return $this->result($response);
    }
}
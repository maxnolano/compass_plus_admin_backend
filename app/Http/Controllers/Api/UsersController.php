<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Users\UsersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UsersController extends ApiController
{

    /**
     * List of users
     * @param Request $request
     * @return Object
     */
    public function list(Request $request): Object {
        $response = (new UsersService($request))->list();
        return $this->result($response);
    }

    /**
     * Create user
     * @param Request $request
     * @return Object
     */
    public function create(Request $request): array {
        $response = (new UsersService($request))->create();
        return $this->result($response);
    }

    /**
     * Update user
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new UsersService($request))->update();
        return $this->result($response);
    }

    /**
     * Delete user
     * @param Request $request
     * @return Object
     */
    public function delete(Request $request): array {
        $response = (new UsersService($request))->delete();
        return $this->result($response);
    }
}
<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Promos\PromosService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SavingsController extends ApiController
{
    /**
     * List of promos
     * @param Request $request
     * @return Object
     */
    public function promos(Request $request): Object {
        $response = (new PromosService($request))->promos(1);
        return $this->result($response);
    }
    
    /**
     * List of promos goods
     * @param Request $request
     * @return Object
     */
    public function promosGoods(Request $request): Array {
        $response = (new PromosService($request))->promosGoods(1);
        return $this->result($response);
    }

    /**
     * List of promos gifts
     * @param Request $request
     * @return Object
     */
    public function promosGifts(Request $request): Array {
        $response = (new PromosService($request))->promosGifts(1);
        return $this->result($response);
    }

    /**
     * List of stores
     * @param Request $request
     * @return Object
     */
    public function stores(Request $request): array {
        $response = (new PromosService($request))->stores();
        return $this->result($response);
    }

    /**
     * List of categories
     * @param Request $request
     * @return Object
     */
    public function categories(Request $request): array {
        $response = (new PromosService($request))->categories();
        return $this->result($response);
    }

    /**
     * List of goods
     * @param Request $request
     * @return Object
     */
    public function goods(Request $request): array {
        $response = (new PromosService($request))->goods();
        return $this->result($response);
    }

    /**
     * Create promo
     * @param Request $request
     * @return Object
     */
    public function create(Request $request): array {
        $response = (new PromosService($request))->create(1);
        return $this->result($response);
    }

    /**
     * Update promo
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new PromosService($request))->update(1);
        return $this->result($response);
    }

    /**
     * Delete promo
     * @param Request $request
     * @return Object
     */
    public function delete(Request $request): array {
        $response = (new PromosService($request))->delete(1);
        return $this->result($response);
    }
}
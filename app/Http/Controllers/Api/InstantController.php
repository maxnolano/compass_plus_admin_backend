<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Promos\PromosService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class InstantController extends ApiController
{

    /**
     * List of promos
     * @param Request $request
     * @return Object
     */
    public function promos(Request $request): Object {
        $response = (new PromosService($request))->promos(2);
        return $this->result($response);
    }

    /**
     * List of stores
     * @param Request $request
     * @return Object
     */
    public function stores(Request $request): array {
        $response = (new PromosService($request))->stores();
        return $this->result($response);
    }

    /**
     * Create promo
     * @param Request $request
     * @return Object
     */
    public function create(Request $request): array {
        $response = (new PromosService($request))->create(2);
        return $this->result($response);
    }

    /**
     * Update promo
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new PromosService($request))->update(2);
        return $this->result($response);
    }

    /**
     * Delete promo
     * @param Request $request
     * @return Object
     */
    public function delete(Request $request): array {
        $response = (new PromosService($request))->delete(2);
        return $this->result($response);
    }
}
<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Notifications\NotificationsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class NotificationsController extends ApiController
{

    /**
     * List of notifications
     * @param Request $request
     * @return Object
     */
    public function list(Request $request): Object {
        $response = (new NotificationsService($request))->list();
        return $this->result($response);
    }

    /**
     * Create notification
     * @param Request $request
     * @return Object
     */
    public function create(Request $request): array {
        $response = (new NotificationsService($request))->create();
        return $this->result($response);
    }

    /**
     * Update notification
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new NotificationsService($request))->update();
        return $this->result($response);
    }

    /**
     * Delete notification
     * @param Request $request
     * @return Object
     */
    public function delete(Request $request): array {
        $response = (new NotificationsService($request))->delete();
        return $this->result($response);
    }

    /**
     * Update token
     * @param Request $request
     * @return Object
     */
    public function updateToken(Request $request): array {
        $response = (new NotificationsService($request))->updateToken();
        return $this->result($response);
    }

    /**
     * Send notification
     * @param Request $request
     * @return Object
     */
    public function sendNotification(Request $request): array {
        $response = (new NotificationsService($request))->sendNotification();
        return $this->result($response);
    }
}
<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Faq\FaqService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FaqController extends ApiController
{

    /**
     * List of faqs
     * @param Request $request
     * @return Object
     */
    public function list(Request $request): Object {
        $response = (new FaqService($request))->list();
        return $this->result($response);
    }

    /**
     * Create faq
     * @param Request $request
     * @return Object
     */
    public function create(Request $request): array {
        $response = (new FaqService($request))->create();
        return $this->result($response);
    }

    /**
     * Update faq
     * @param Request $request
     * @return Object
     */
    public function update(Request $request): array {
        $response = (new FaqService($request))->update();
        return $this->result($response);
    }

    /**
     * Delete faq
     * @param Request $request
     * @return Object
     */
    public function delete(Request $request): array {
        $response = (new FaqService($request))->delete();
        return $this->result($response);
    }
}
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmProducts extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title_p' => $this->title_p,
            'item_code' => $this->item_code,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'weight' => $this->weight,
            'weight_unit' => $this->weight_unit,
            'ean13' => $this->ean13,
            'img' => $this->img,
        ];
    }
}

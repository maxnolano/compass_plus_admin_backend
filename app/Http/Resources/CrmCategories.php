<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmCategories extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id_c' => $this->id_c,
            'title_c' => $this->title_c,
            'store_id_c' => $this->store_id_c,
            'object_type_c' => $this->object_type_c,
        ];
    }
}

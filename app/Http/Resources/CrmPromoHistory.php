<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmPromoHistory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'username' => $this->username,
            'promo_id' => $this->promo_id,
            'promo_title' => $this->promo_title,
            'created_at' => $this->created_at,
            'ip_address' => $this->ip_address,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class CrmPromo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            // 'st_store_id' => $this->st_store_id,
            // 'st_store_name' => $this->st_store_name,
            'title' => $this->title,
            'description' => $this->description,
            'expire_date' => $this->expire_date,
            'status' => $this->status,
        ];
    }
}

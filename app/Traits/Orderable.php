<?php

namespace App\Traits;

trait Orderable {
    public function scopeDesc($query) {
        return $query->orderBy('create_date', 'desc');
    }

    public function scopeAsc($query) {
        return $query->orderBy('create_date', 'asc');
    }
}